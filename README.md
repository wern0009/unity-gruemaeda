# **Controls**

Moving the **Grue**: **Arrow Keys**

Moving the **Fleche** up and down: **W and S**

Moving the **Fleche** left and right: **Y and X**

Extending/Retracting the **Chariot**: **A and D**

Moving the **legs** out/in: **T and G**

Lowering/Lifting the **legs**: **Z and H**

Lowering/Lifting the **hook**: **R and F**

Dropping the **cube**: **Space bar**