using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 // The movement buttons are set in the input manager.
 // For moving the mobile, use the arrow keys. W and S lift up the Fleche. A and D allow you to extend the Fleche/Chariots
 // R and F allows you to let the hook down and up. Y and X make the Fleche move left and right. G and T make the legs go out and in. Z and H make the legs go down and up.
public class Movement : MonoBehaviour
{
    public float moveSpeed = 0.01f;  // Speed for forward/backward movement
    public float rotationSpeed = 2.0f;  // Speed for turning

    // Update is called once per frame
    void Update() 
    {
        // Forward/backward movement along the Y-axis (since Y is your forward direction)
        if (Input.GetKey(KeyCode.UpArrow)) 
        {
            transform.Translate(Vector3.up * moveSpeed); // Move forward (Y-axis)
        }
        if (Input.GetKey(KeyCode.DownArrow)) 
        {
            transform.Translate(Vector3.down * moveSpeed); // Move backward (Y-axis)
        }

        // Rotation around the Z-axis (since Z is up, you'll rotate around Z for turning)
        if (Input.GetKey(KeyCode.RightArrow)) 
        {
            transform.Rotate(Vector3.forward, rotationSpeed); // Rotate left
        }
        if (Input.GetKey(KeyCode.LeftArrow)) 
        {
            transform.Rotate(Vector3.forward, -rotationSpeed); // Rotate right
        }
    }
}