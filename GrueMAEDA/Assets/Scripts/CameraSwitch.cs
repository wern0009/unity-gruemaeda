using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSwitch : MonoBehaviour
{
    // Array to hold references to the cameras
    public Camera[] cameras;
    
    // Current active camera index
    private int currentCameraIndex = 0;

    void Start()
    {
        // Activate only the first camera at the start
        ActivateCamera(currentCameraIndex);
    }

    void Update()
    {
        // If 'Q' is pressed, switch to the previous camera
        if (Input.GetKeyDown(KeyCode.Q))
        {
            currentCameraIndex--; // Move to the previous camera
            if (currentCameraIndex < 0)
            {
                currentCameraIndex = cameras.Length - 1; // If we go past the first camera, loop to the last one
            }
            ActivateCamera(currentCameraIndex);
        }

        // If 'E' is pressed, switch to the next camera
        if (Input.GetKeyDown(KeyCode.E))
        {
            currentCameraIndex++; // Move to the next camera
            if (currentCameraIndex >= cameras.Length)
            {
                currentCameraIndex = 0; // If we go past the last camera, loop back to the first one
            }
            ActivateCamera(currentCameraIndex);
        }
    }

    // Function to activate the selected camera and deactivate the rest
    void ActivateCamera(int index)
    {
        // Loop through all cameras, only activate the one at the given index
        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].gameObject.SetActive(i == index); // Activate the camera that matches the index
        }
    }
}
