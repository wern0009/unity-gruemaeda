using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappin : MonoBehaviour
{
    // This function is called when the object collides with another object
    void OnCollisionEnter(Collision Collision)
    {
        // Check if the object we hit has an ArticulationBody component
        if (Collision.gameObject.GetComponent<ArticulationBody>() != null)
        {
            // Add a FixedJoint to this object and connect it to the other object's articulation body
            FixedJoint joint = this.gameObject.AddComponent<FixedJoint>();
            joint.connectedArticulationBody = Collision.articulationBody;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        // This is empty for now
    }

    // Update is called once per frame
    void Update()
    {
        // If the space key is pressed, destroy the FixedJoint component
        if (Input.GetKey(KeyCode.Space))
        {
            Destroy(this.gameObject.GetComponent<FixedJoint>());
        }
    }
}
